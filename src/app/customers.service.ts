import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Customer } from './interfaces/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

userCollection:AngularFirestoreCollection = this.db.collection('users');
customersCollection:AngularFirestoreCollection;

getCustomers(userId): Observable<any[]> {
  this.customersCollection = this.db.collection(`users/${userId}/customers`,
     ref => ref.limit(10))
  return this.customersCollection.snapshotChanges();
}

updateRsult(userId:string, id:string,result:string){
  this.db.doc(`users/${userId}/customers/${id}`).update(
    {
      result:result
    })
  }

updateCustomer(userId:string, id:string,name:string,years:number,income:number){
  this.db.doc(`users/${userId}/customers/${id}`).update(
    {
      name:name,
      years:years,
      income:income,
      result:null
    }
  )
}

deleteCustomer(userId:string, id:string){
  this.db.doc(`users/${userId}/customers/${id}`).delete();
}

addCustomer(userId:string, name:string, years:number, income:number){
  const customer:Customer = {name:name,years:years, income:income}
  this.userCollection.doc(userId).collection('customers').add(customer);
}



constructor(private db: AngularFirestore) {}

}

