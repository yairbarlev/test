import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Comments } from './interfaces/comments';
import { Post } from './interfaces/post';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  private URL = "https://jsonplaceholder.typicode.com/posts/";
  private URL2 = "https://jsonplaceholder.typicode.com/comments?";

  userCollection:AngularFirestoreCollection = this.db.collection('users');
  savedPostsCollection:AngularFirestoreCollection;
  constructor(private http: HttpClient, private db: AngularFirestore) {}

  getSavedPosts(userId:string): Observable<any[]> {
    this.savedPostsCollection = this.db.collection(`users/${userId}/posts`)
    return this.savedPostsCollection.snapshotChanges();
  }

  savePost(userId:string, postId:number , title:string ,body:string){
    const post:Post = { userId:userId ,id: postId, title:title, body:body}
  this.userCollection.doc(userId).collection('posts').add(post);
  }

  getPosts():Observable<Post>{
    return this.http.get<Post>(this.URL);
  }

  getComments():Observable<Comments>{
    return this.http.get<Comments>(this.URL2);
  }

}
