export interface Student {
  id?:string;
  useremail: string;
  mathG: number;
  psyG:number;
  paid:number;
  result:number;
}
