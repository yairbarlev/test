import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../interfaces/post';
import { BlogService } from '../blog.service';
import { Comments } from '../interfaces/comments';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
  saveError = false;
  errorMessage;
  comments$;
  userId;
  posts$:Observable<Post>
  constructor(private blogService:BlogService  ,public authService:AuthService,) { }


  public savepost(id:number,title:string,body:string){
    this.blogService.savePost(this.userId ,id,title,body);
  }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
      }
    )
    this.posts$ = this.blogService.getPosts();
    this.comments$ = this.blogService.getComments();
  }


}
