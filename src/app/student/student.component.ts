import { Component, OnInit, Input,  Output, EventEmitter } from '@angular/core';
import { Student } from '../interfaces/student';
import {FormControl} from '@angular/forms';
import { PredictionService } from '../prediction.service';
import { StudentService } from '../student.service';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  @Input() mathG: number;
  @Input() psyG: number;
  @Input() paid: string;
  // @Input() formType: string;

  @Output() update = new EventEmitter<Student>();
  @Output() closeEdit = new EventEmitter<null>();

  predicted:boolean;
  result;
  message;
  userId;
  usermail;
  username;
  opisiteOf;
  paidList: string[] = ['Did not pay','Paid'];
  passList= [

    { value: '0', label: 'Will not pass', selected: true },

    { value: '1', label: 'Pass' },

    ];
  buttonText:String = 'Predict';
  isValid:boolean = false;
  validPsy = false;
  validMath = false;
  onSubmit(){

  }

  tellParentToClose(){
    this.closeEdit.emit();
  }
  cancel(){
    this.result = '';
    this.predicted = false;
  }
  save( ){
    if(this.paid == '1'){
    this.studentService.addStudent(this.usermail, this.mathG, this.psyG, 1, this.result)
    this.message = "Saved for later viewing"}
    else{
      this.studentService.addStudent(this.usermail, this.mathG, this.psyG,0, this.result)
      this.message = "Saved for later viewing"
    }
 }
  predict(){
    if(this.psyG<200 || this.psyG>800 ){
      this.validPsy = true;
      if(this.mathG<0 || this.mathG>100 ){
        this.validMath = true;
      }
    }
    else{
      if(this.mathG<0 || this.mathG>100 ){
        this.validMath = true;
      }
      else{
        if(this.paid == '1'){
          this.predictionService.predict(this.mathG, this.psyG, 1).subscribe(
            res => {console.log(res);
              if(res > 0.5){
                var result = 'Will pass';
              } else {
                var result = 'Will fail'
              }
              this.result = result}
          );
        }
        else{
          this.predictionService.predict(this.mathG, this.psyG, 0).subscribe(
            res => {console.log(res);
              if(res > 0.5){
                var result = 'Will pass';
              } else {
                var result = 'Will fail'
              }
              this.result = result}
          );
        }
        this.predicted = true;
        this.validMath = false;
        this.validPsy = false;
      }
    }

  }

  constructor(private predictionService:PredictionService , private studentService:StudentService,private auth:AuthService) { }


  ngOnInit(): void {
      this.buttonText = 'Add';
      this.auth.user.subscribe(
        user => {
          this.userId = user.uid;
          this.usermail = user.email;
          this.username = user.displayName;
         }
      )
  }

}
