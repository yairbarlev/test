import { Student } from './interfaces/student';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  studentCollection:AngularFirestoreCollection  = this.db.collection('students');;

  addStudent(useremail,mathG, psyG, paid, result){
    const student:Student = { useremail:useremail ,mathG:mathG,psyG:psyG,paid:paid, result:result};
    this.studentCollection.add(student);
    this.router.navigate(['/list']);
  }
  studentsCollection:AngularFirestoreCollection;


  deleteStudent(id:string){
    this.db.doc(`/students/${id}`).delete();
  }
getStudents(): Observable<any[]> {
  this.studentsCollection  = this.db.collection('students');
  return this.studentsCollection.snapshotChanges();
}
  constructor(private db: AngularFirestore,private router:Router) { }
}
