import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictionService {
  url = 'https://wk60kribye.execute-api.us-east-1.amazonaws.com/beta/';

  predict(mathG:number, psyG:number,paid:number ):Observable<any>{
    let json = {
      "data":
        {
          "mathG": mathG,
          "psyG": psyG,
          "paid" : paid
        }
    }
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map");
        console.log(res);
        console.log(res.body);
        return res.body;
      })
    );
  }


  constructor(private http: HttpClient) { }
}
