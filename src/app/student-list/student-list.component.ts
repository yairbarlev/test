
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Student } from '../interfaces/student';
import { StudentService } from '../student.service';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.css']
})
export class StudentListComponent implements OnInit {
  students$;
  students:Student[];
  userId:string;
  editstate = [];
  id = [];
  constructor(private studentService:StudentService, public authService:AuthService) { }

  displayedColumns: string[] = [ 'User email', 'Math grade','Psy grade', 'paid', 'Result', 'Delete'];

  deleteStudent(index){
    console.log(this.id[index]);
    console.log(index);
    this.studentService.deleteStudent(this.students[index].id);
  }
  ngOnInit(): void {
    this.students$ = this.studentService.getStudents();

    this.students$.subscribe(
            docs => {
              this.students = [];
              this.id = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++);
                const student:Student = document.payload.doc.data();
                student.id = document.payload.doc.id;
                console.log(this.id[i] + 'dhkjdf');
              this.students.push(student);;
              }
            }
          )
      }

}
