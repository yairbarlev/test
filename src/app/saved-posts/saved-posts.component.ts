import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { BlogService } from '../blog.service';
import { Post } from '../interfaces/post';

@Component({
  selector: 'app-saved-posts',
  templateUrl: './saved-posts.component.html',
  styleUrls: ['./saved-posts.component.css']
})
export class SavedPostsComponent implements OnInit {
  userId;
  savedposts$;
  savedposts:Post[];
  constructor(private blogService:BlogService, public authService:AuthService) {


  }
  // getSavedPosts()
  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          console.log(user.uid);
          this.savedposts$ = this.blogService.getSavedPosts(user.uid);
          this.savedposts$.subscribe(
            docs =>{
              console.log('init worked');
              this.savedposts = [];
              for(let document of docs){
                const post:Post = document.payload.doc.data();
                post.id = document.payload.doc.id;
                this.savedposts.push(post);
              }
            }
          )
      }
    )

  }

}
