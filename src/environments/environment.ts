// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyCHkxbsvufyBhV8sqAgFVZmFMtLI5GjJIs",
    authDomain: "hometest-b50a3.firebaseapp.com",
    projectId: "hometest-b50a3",
    storageBucket: "hometest-b50a3.appspot.com",
    messagingSenderId: "1053233104543",
    appId: "1:1053233104543:web:c53f4548ce9463c6a504aa"

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
